const exp = require('../exports')
class Logger {

    constructor () {

        this.lastcontent;
        this.lastservice;

    }

    log(content, service, hide) { // logs every thing to a log file
        if(!service) throw `${exp.color.fg.red}[ERROR]${exp.color.reset} LOG FUNC NEEDS SERVICE ARGUMENT.`
        if(hide != true) {
            console.log(`${exp.color.fg.yellow}[${service}]${exp.color.reset} ${content}`)
        }           
        this.lastcontent = content
        this.lastservice = service
        return this;
    };

    save() {

        try {
            let a = this.last_log
            fs.appendFileSync(`./logs/${main.date()}.txt`, `\n [${main.timestamp()} >> ${a.service}] ${a.content}`, error => {
                // logging function
                if (error) {
                    console.error("Error on Logging: " + error);
                    process.exit("LOG_ERROR");
                }
            });
        } catch {
            if (fs.existsSync('./logs')) {
                fs.writeFile(`./logs/${main.date()}.txt`, "log created", error => {
                    // logging function
                    if (error) {
                        console.error("Error on Logging: " + error);
                        process.exit("LOG_ERROR");
                    }
                });
                return
            } else {
                fs.mkdirSync('./logs');
                fs.writeFile(`./logs/${main.date()}.txt`, "log created", error => {
                    // logging function
                    if (error) {
                        console.error("Error on Logging: " + error);
                        process.exit("LOG_ERROR");
                    }
                });
                return
            }
        }
    }


}

module.exports = {

    Logger

}