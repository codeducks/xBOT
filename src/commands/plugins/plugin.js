const Discord = require('discord.js');
const axios = require('axios')
const fs = require('fs')

module.exports.run = async (bot, message, args) => {
    //this is where the actual code for the command goes
    if (message.author.id != process.env.OWNER)

  async function downloadPlugin () {  
    const url = 'https://unsplash.com/photos/AaEQmoufHLk/download?force=true'
    const path = Path.resolve(__dirname, 'plugins', `${args[0]}.zip`)
    const writer = Fs.createWriteStream(path)
  
    const response = await Axios({
      url,
      method: 'GET',
      responseType: 'stream'
    })
  
    response.data.pipe(writer)
  
    return new Promise((resolve, reject) => {
      writer.on('finish', resolve)
      writer.on('error', reject)
    })
  }
}

//name this whatever the command name is.
module.exports.help = {
  name: 'x',
  aliases: []
}