const db = require("../../data/database.json");
const Discord = require('discord.js');
const env = require('dotenv');
const exp = require('../../exports');
const config = require("../../utils/global.json");

module.exports.run = async (bot, message, args) => {
  //this is where the actual code for the command goes
  const embed = new Discord.MessageEmbed;
  if (message.member.hasPermission('ADMINISTRATOR')) {

    if (args.length > 1) {
        message.channel.send("no spaces are allowed in a prefix.")
        return;
    }

    if (args.length < 1) {
    message.reply("please specify a new prefix")
    return;}

    db.guilds[message.guild.id].prefix = args[0]
    embed.setTitle("New Prefix");
    embed.setColor('RANDOM');
    embed.setDescription("The new prefix: " + args[0]);
    message.channel.send(embed);


  } else return;
}

//name this whatever the command name is.
module.exports.help = {
  name: 'setprefix',
  aliases: []
}