const Discord = require('discord.js');
  const embed = new Discord.MessageEmbed();
  
  const db = require('../../data/database.json').eco
  
  module.exports.run = async (bot, message, args) => {

    if (isNaN(args[0])) {
      message.reply("please specify how many coins you would like to destroy!");
      return;
    }
    if (message.member.nickname != null) {
      authorUsername = message.member.nickname
    } else {
      authorUsername = message.author.username
    }
    embed.setColor('RANDOM');
    embed.setTitle('Free Gold?');
    db[message.author.id]
    if (parseInt(args[0]) > db[message.author.id]) {
      embed.setDescription(`You don't have that many coins`)
      } else {
        db[message.author.id] = db[message.author.id] - parseInt(args[0])
        embed.setDescription(`${authorUsername} destroyed ${args[0]} coins`)
      }
    message.channel.send(embed);
  }
  
  //name this whatever the command name is.
  module.exports.help = {
    name: "melt",
    aliases: ["destroy", "drop"]
  }