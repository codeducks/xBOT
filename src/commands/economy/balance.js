const Discord = require('discord.js');
const embed = new Discord.MessageEmbed();
const db = require('../../data/database.json').eco

module.exports.run = async (bot, message, args) => {
  if(db[message.author.id] === undefined) {
    db[message.author.id] = 0
  }

  embed.setColor('RANDOM');
  embed.setTitle("Balance");
  embed.setDescription(`${message.author.username} has ${db[message.author.id]} coins`)
  
  message.reply(embed);
}

//name this whatever the command name is.
module.exports.help = {
  name: "balance",
  aliases: ["bal"]
}