const express = require('express');
const app = require('express')();
const config = require("../utils/global.json");
const api = require('../data/api.json')
const exp = require('../exports')
const cfg = require('./configs/api.json')

exports.run = () => {
    
    app.use(express.static("./views/static", { extensions: "html"})); // static pages.
    // app.use(express.static("./commands", {extensions: "js"})); // ? see code from your bot online! (only from "./commands")

    app.use((req, res, next) => {
        const auth = req.headers.authorization;
        if(api.ip_wl.includes(req.ip) == true) {
            next();
            return
        }
        if(!auth || api.tokens.includes(auth) == false) {
            res.sendStatus(401)
            return
        }
        next();
    })

    

    try {
        app.listen(cfg.port, cfg.hostname)
    } catch (err) {
        exp.log(`could start on specified port. ${exp.color.fg.red}error:${exp.color.reset} ` + err, 'API', false)
    }
}

exports.info = {

    "name": "API"

}