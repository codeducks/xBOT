const Discord = require("discord.js")
require('dotenv').config();
const global = require("./utils/global.json")
const fs = require("fs")
const bot = new Discord.Client();
const exp = require('./exports');
const http = require('http');
const { config } = require("dotenv");
const { cache } = require("ejs");
const db = require('./data/database.json')
const defaulty = require('./utils/default.json')

bot.commands = new Discord.Collection();
bot.aliases = new Discord.Collection();

if(process.env.TOKEN === "setmeplease" || !process.env.TOKEN) return console.log("[!] Set your token up! Go to https://www.discordapp.com/developers and generate a token from a bot user.");

// stops the bot after 30 seconds.
if (process.argv.slice(2) == 'ci') {

  setTimeout(() => {
    process.exit(0);
  }, 30 * 100)

}

console.clear();
console.log(exp.color.fg.magenta + exp.color.special.blink + "Loading..." + exp.color.reset)

bot.on("ready", async () => {

  console.clear()
  console.log('"' + exp.color.fg.yellow + exp.color.special.underscore + bot.user.username + exp.color.reset + '"' + " CONSOLE" + exp.color.reset)
  console.log("- SERVICES -")
  
  m = exp.botStart();

  if (m.includes("guilds") == true) {

    const cache = bot.guilds.cache.map(guild => guild.id);
    cache.forEach(i => {
      db.guilds[i].prefix = global.prefix
    });

  }

  bot.user.setActivity(global.name, {
    type: global.presence // ! WATCHING, STREAMING, LISTENING OR PLAYING set it in global.json
  });

  global.defaultservices.forEach((f, i) => {
    exp.service_load(f)
  })

  console.log("- COMMANDS -");

  global.defaultmodules.forEach( async (f, i) =>{
    await exp.load(f, "./commands")
  });

});

bot.on('guildCreate', (guild) => {
  db.guilds[guild.id] = defaulty.guild
})

function ruled(a) {
  if (a == false) return "allowed"
  if (a == true) return "disallowed"
}

function sanitise(message) {
  if (global.sanitise.wordfilter == true) {
      if (wash.check('en', message.content) == true) {
          var deletemsg = true
      }
  }

  if (global.sanitise.capsfilter == true) {
      if (message.content === message.content.toUpperCase()) {
          var deletemsg = true;
      }    
  }

  
}

bot.on("message", async message => {
  //a little bit of data parsing/general checks
  if(message.author.bot) return;
  if(message.channel.type === 'dm') return; // ? comment out if you want to enable commands in DMs.

  if (sanitise(message) == true) {
    message.delete();
    const embed = new Discord.MessageEmbed();
    embed.setTitle("Whoops!")
    embed.setDescription("Here are the chat filters!")
    embed.addField("Profanity", ruled(global.sanitise.wordfilter), true)
    embed.addField("Caps", ruled(global.sanitise.capsfilter), true)

    message.channel.send(embed)

  }

  if (db.guilds.hasOwnProperty(message.guild.id) == false) {
    db.guilds[message.guild.id] = defaulty.guild
  }

  let content = message.content.toLowerCase().split(" ");
  let command = content[0];
  let args = content.slice(1);
  let prefix = db.guilds[message.guild.id].prefix

  if (message.content.indexOf(prefix) !== 0) return;

  if (!bot.commands.get(command.slice(prefix.length)) && !bot.aliases.get(command.slice(prefix.length))) {
   message.channel.send(exp.buildembed('Command not found', 
   "Sorry, this command either does not exist or wasn't loaded.", 
   '"' + prefix + 'help" for help',
   true));
  }

  const commandfile = bot.commands.get(command.slice(prefix.length)) || bot.commands.get(bot.aliases.get(command.slice(prefix.length)));
  if(commandfile) commandfile.run(bot,message,args);
  fs.writeFileSync('./data/database.json', JSON.stringify(db))
})

exports.loadCommand = function(a, b){bot.commands.set(a, b)} // for adding commands to the bot
exports.loadAlias = function(c, d){bot.aliases.set(c, d)} // for adding aliases to the bot
exports.reload = function(arg, mod) {
  console.clear();
  bot.commands = new Discord.Collection();
  bot.aliases = new Discord.Collection();
  console.log("Bot was reloaded.");
  console.log("------------");

  switch (arg) {
    case '--module':
    case '-m': 
    exp.load("default");
    exp.load(mod);
    break;

    case '--nothing':
    case '-n':
    break;

    case '--only':
    case '-o':
    exp.load(mod);
    break;

    default:
      exp.load("default");
    break;
  }
  
}
bot.login(process.env.TOKEN) // your token can be specified in the .env file