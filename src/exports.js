// lots of depends, i know.

const fs = require("fs");
const Discord = require('discord.js');
const main = require("./exports");
const config = require("./utils/global.json");
const cmds = require('./utils/commands.json');
require("dotenv").config();
const crypto = require("crypto");
const got = require('got');
const db = require('./data/database.json')
const prof = require('washyourmouthoutwithsoap')
const Logger = require('./utils/logger').Logger;
const log = new Logger().log;

exports.c_cmd = 0;

exports.botStart = function () { // will be run on bot "ready".

    missing = []

    if (!db.guilds) {

        main.log(`Created guilds table for guild-specific data.`, 'DB');
        db.guilds = new Object();
        missing.push("guilds")

    }

    if (config.useeco == true || config.api.enabled == true) {

        if (!db.eco) {
            console.log(`[ECO] Created 'main.db' for economy module.`);
            console.log(`[ECO] If you have the module disabled then this wont do anything`);
            db.eco = new Object();
            missing.push("eco")
        }

        fs.stat('./data/api.json', (err, stat) => {

            if (err) {
                fs.appendFileSync('./data/api.json', '{}');
                missing.push('api')
            }

        })

    }

    return missing;

    // ! BETA. 
    // uncomment if on heroku
    // setInterval(function() {got(global.heroku)}, 300 * 1000)
    // every 300 seconds. (5 minutes)

}

exports.timestamp = () => {new Date().toLocaleTimeString()}
exports.date = () => {new Date().toLocaleDateString()}
exports.log = Logger;

exports.stats = () => {
    // logging function
    try {
        fs.appendFileSync(`./logs/stats/${main.date()}.txt`, `${content}\n`, error => {
            // logging function
            if (error) {
                console.error("Error on storing stats: " + error);
                process.exit("LOG_ERROR");
            }
            return;
        });
    } catch {
        if (fs.existsSync('./logs/stats')) {
            fs.writeFile(`./logs/stats/${main.date()}.txt`, "log created", error => {
                // logging function
                if (error) {
                    console.error("Error when storing stats: " + error);
                    process.exit("LOG_ERROR");
                }
            });
            return;
        } else {
            fs.mkdirSync('./logs/stats');
            fs.writeFile(`./logs/stats/${main.date()}.txt`, "stat file created", error => {
                // logging function
                if (error) {
                    console.error("Error on storing stats: " + error);
                    process.exit("LOG_ERROR");
                }
            });
            return;
        }
    }
    return;
};

exports.hash = (content, type) => {
    return crypto.createHash(type).update(content).digest("hex");
};

exports.check = (folder) => {
    if (fs.existsSync(`./commands/${folder}`)) {
        return true;
    } else {
        return false;
    }
};

exports.load = async (folder, src) => {

    const index = require('./index')

    if (fs.existsSync(`${src}/${folder}`)) {
        fs.readdir(`${src}/${folder}`, (err, files) => {
            if (err) console.log(err);

            let jsfile = files.filter(f => f.split(".").pop() === "js");

            if (jsfile.length <= 0) {
                console.log(`${this.color.fg.red}[ERR]${this.color.reset} Couldn't find commands.`);
                return;
            }

            jsfile.forEach((f, i) => {
                let props = require(`${src}/${folder}/${f}`);
                index.loadCommand(props.help.name, props);
                props.help.aliases.forEach(alias => {
                    index.loadAlias(alias, props.help.name);
                });
                console.log(`${this.color.fg.yellow}[BOT]${this.color.fg.blue} ${folder}/${this.color.special.bright}${f} ${this.color.reset}loaded!`);
                this.c_cmd++
            });
        })
    } else {
        this.log(this.color.fg.red + 'tried to load ' + folder + ' but could not find it.' + this.color.reset, 'ERROR')
    }
};

exports.service_load = (service) => {

    if(fs.existsSync(`./services/${service}.js`) == false) {
        this.log("loaded", service, false)

    }
    const props = require(`./services/${service}`)
    props.run();
    log(`loaded.`, props.info.name)

}

exports.buildembed = (titleText, messageText, footerText, timestampBool) => {

    const embed = new Discord.MessageEmbed();

    embed.setColor('RANDOM');
    embed.setTitle(titleText);
    embed.setDescription(messageText);
    embed.setFooter(footerText);
    if (timestampBool == true) embed.setTimestamp();

    return embed

}

exports.formatter = (unformattedString) => {

    // if it is an array it will make it one.
    if (unformattedString.includes('[')) {
        var v1 = unformattedString.replace('[', '')
        var v2 = v1.replace(']', '')
        var array = v2.split(',')
        return array
    }

    // true or false
    switch (unformattedString) {

        case 'true':
            return true;

        case 'false':
            return false;

        default:
            // return string..
            return unformattedString

    }

}

exports.color = {
    reset: "\x1b[0m",
    
    special: {
        bright: "\x1b[1m",
        dim: "\x1b[2m",
        underscore: "\x1b[4m",
        blink: "\x1b[5m",
        reverse: "\x1b[7m",
        hidden: "\x1b[8m"
    },

    fg: {
        black: "\x1b[30m",
        red: "\x1b[31m",
        green: "\x1b[32m",
        yellow: "\x1b[33m",
        blue: "\x1b[34m",
        magenta: "\x1b[35m",
        cyan: "\x1b[36m",
        white: "\x1b[37m"
    },

    bg: {
        black: "\x1b[40m",
        red: "\x1b[41m",
        green: "\x1b[42m",
        yellow: "\x1b[43m",
        blue: "\x1b[44m",
        magenta: "\x1b[45m",
        cyan: "\x1b[46m",
        white: "\x1b[47m"
    }
}