const conf = require('conf'),
      inq = require('inquirer'),
      fig = require('figlet'),
      pm2 = require('pm2')

inq.prompt({
    name: 'xBOT',
    prefix: fig.textSync('xBOT') + '\n',
    type: 'list',
    choices: ['Start Daemon', 'Stop Daemon', 'Advanced']
}).then((answers) => {

    switch (answers.xBOT) {

        case 'Start Daemon':
            pm2.connect(err => {
                if (err) throw err;
                pm2.start({name: 'XBOT-DAEMON', script: `${__dirname}/index.js`, cwd: __dirname})
                pm2.disconnect();
            })
        break;

        case 'Stop Daemon':
            pm2.connect(err => {
                if(err) throw err;
                pm2.stop('XBOT-DAEMON')
                pm2.disconnect();
            })

    }

})